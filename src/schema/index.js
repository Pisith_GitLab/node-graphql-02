import { SchemaCompose, SchemaComposer } from 'graphql-compose';
import { db } from '../utils/db';

const schemaComposer = new SchemaComposer();

import { UserQuery, UserMutation } from '../schema/user';
import { TaskQuery, TaskMutation } from '../schema/task';

schemaComposer.Query.addFields({
    ...UserQuery,
    ...TaskQuery,
});

schemaComposer.Mutation.addFields({
    ...UserMutation,
    ...TaskMutation,
});

export default schemaComposer.buildSchema();