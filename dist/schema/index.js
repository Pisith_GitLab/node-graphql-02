"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _graphqlCompose = require("graphql-compose");

var _db = require("../utils/db");

var _user = require("../schema/user");

var _task = require("../schema/task");

const schemaComposer = new _graphqlCompose.SchemaComposer();
schemaComposer.Query.addFields({ ..._user.UserQuery,
  ..._task.TaskQuery
});
schemaComposer.Mutation.addFields({ ..._user.UserMutation,
  ..._task.TaskMutation
});
exports.default = schemaComposer.buildSchema();